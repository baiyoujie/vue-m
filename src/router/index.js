import Vue from 'vue'
import Router from 'vue-router'
import A1 from '@/components/A1'
import A2 from '@/components/A2'
import A3 from '@/components/A3'
import A4 from '@/components/A4'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'name1',
      component: A1
    },
    {
      path: '/2',
      name: 'name2',
      component: A2
    },
    {
      path: '/3',
      name: 'name3',
      component: A3
    },
    {
      path: '/4',
      name: 'name4',
      component: A4
    },
  ]
})
